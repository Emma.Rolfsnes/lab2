package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
    int max_size = 20;
    public ArrayList<FridgeItem> item,
    items = new ArrayList<FridgeItem>();


    @Override
    public int nItemsInFridge() {
        return items.size();
    }

    @Override
    public int totalSize() {
        return max_size;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (items.size() >= totalSize())
            return false;
        return items.add(item);
    }

    @Override
    public void takeOut(FridgeItem item) {
        if(!items.contains(item))
            throw new NoSuchElementException("Item not in fridge");
        items.remove(item);
    }

    @Override
    public void emptyFridge() {
        items.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
       ArrayList<FridgeItem> ExpiredFood = new ArrayList<>();
       for(int i = 0; i < nItemsInFridge(); i++){
           FridgeItem item = items.get(i);
           if(item.hasExpired()) {
               ExpiredFood.add(item);
           }
       }
       for (FridgeItem ExpiredItem : ExpiredFood) {
           items.remove(ExpiredItem);
       }
        return ExpiredFood;
    }
}
